// Copyright 2012 Intel Corporation
//
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// - Redistributions of source code must retain the above copyright notice, this
//   list of conditions and the following disclaimer.
//
// - Redistributions in binary form must reproduce the above copyright notice,
//   this list of conditions and the following disclaimer in the documentation
//   and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#pragma once

#ifdef WAFFLE_CMAKE
#include <stdio.h>
#endif

#include <stdbool.h>
#include <stdint.h>
#include "c99_compat.h"

struct wcore_config;
struct wcore_config_attrs;
struct wcore_context;
struct wcore_display;
struct wcore_platform;
struct wcore_window;

struct wcore_platform_vtbl {
    bool
    (*destroy)(struct wcore_platform *self);

    bool
    (*make_current)(
            struct wcore_platform *self,
            struct wcore_display *dpy,
            struct wcore_window *window,
            struct wcore_context *ctx);

    void*
    (*get_proc_address)(
            struct wcore_platform *self,
            const char *proc);

    bool
    (*dl_can_open)(
            struct wcore_platform *self,
            int32_t waffle_dl);

    void*
    (*dl_sym)(
            struct wcore_platform *self,
            int32_t waffle_dl,
            const char *symbol);

    struct wcore_display_vtbl {
        struct wcore_display*
        (*connect)(struct wcore_platform *platform,
                   const char *name);

        bool
        (*destroy)(struct wcore_display *self);

        bool
        (*supports_context_api)(
                struct wcore_display *display,
                int32_t context_api);

        /// May be null.
        union waffle_native_display*
        (*get_native)(struct wcore_display *display);
    } display;

    struct wcore_config_vtbl {
        struct wcore_config*
        (*choose)(struct wcore_platform *platform,
                  struct wcore_display *display,
                  const struct wcore_config_attrs *attrs);

        bool
        (*destroy)(struct wcore_config *config);

        /// May be null.
        union waffle_native_config*
        (*get_native)(struct wcore_config *config);
    } config;

    struct wcore_context_vtbl {
        struct wcore_context*
        (*create)(struct wcore_platform *platform,
                  struct wcore_config *config,
                  struct wcore_context *share_ctx);

        bool
        (*destroy)(struct wcore_context *ctx);

        /// May be null.
        union waffle_native_context*
        (*get_native)(struct wcore_context *ctx);
    } context;

    struct wcore_window_vtbl {
        struct wcore_window*
        (*create)(struct wcore_platform *platform,
                  struct wcore_config *config,
                  int32_t width,
                  int32_t height,
                  const intptr_t attrib_list[]);
        bool
        (*destroy)(struct wcore_window *window);

        bool
        (*show)(struct wcore_window *window);

        bool
        (*swap_buffers)(struct wcore_window *window);

        bool
        (*resize)(struct wcore_window *window,
                  int32_t height,
                  int32_t width);

        /// May be null.
        union waffle_native_window*
        (*get_native)(struct wcore_window *window);
    } window;
};

struct wcore_dl {
    /// @brief For example, "libGLESv2.so.2".
    const char *name;

    /// @brief The library obtained with dlopen().
    ///
    /// The library is initialized if and only if `dl != NULL`.
    void *handle;
};

struct wcore_platform {
    const struct wcore_platform_vtbl *vtbl;
    enum waffle_enum waffle_platform; // WAFFLE_PLATFORM_*
    struct wcore_dl gl;
    struct wcore_dl gles1;
    struct wcore_dl gles2;
};

// XXX: Returns non-null, annotate as such.
static inline const char *
wcore_name_from_waffle_dl(int32_t waffle_dl)
{
    switch (waffle_dl) {
    case WAFFLE_DL_OPENGL:
        return "OpenGL";
    case WAFFLE_DL_OPENGL_ES1:
        return "OpenGL ES1";
    case WAFFLE_DL_OPENGL_ES2:
        return "OpenGL ES2";
    case WAFFLE_DL_OPENGL_ES3:
        return "OpenGL ES3";
    default:
        // XXX: shut-up static analysis tools.
        // There should be a better way.
        return NULL;
    }
}

// XXX: takes a non-null, annotate as such
static inline struct wcore_dl *
wcore_dl_from_waffle_dl(struct wcore_platform *self, int32_t waffle_dl)
{
    switch (waffle_dl) {
    case WAFFLE_DL_OPENGL:
        return &self->gl;
    case WAFFLE_DL_OPENGL_ES1:
        return &self->gles1;
    case WAFFLE_DL_OPENGL_ES2:
    case WAFFLE_DL_OPENGL_ES3:
        return &self->gles2;
    default:
        // XXX: shut-up static analysis tools.
        // There should be a better way.
        return NULL;
    }
}

static inline void
wcore_platform_init(struct wcore_platform *self)
{
#ifdef WAFFLE_CMAKE
    fprintf(stderr, "Using the deprecated cmake build. Please switch to the "
                    "meson build or open a bug report.");
#endif
}
