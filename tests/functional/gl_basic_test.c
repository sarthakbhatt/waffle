// Copyright 2012 Intel Corporation
//
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// - Redistributions of source code must retain the above copyright notice, this
//   list of conditions and the following disclaimer.
//
// - Redistributions in binary form must reproduce the above copyright notice,
//   this list of conditions and the following disclaimer in the documentation
//   and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

/// @file
/// @brief Test basic OpenGL rendering with all platform/gl_api combinations.
///
/// Each test does the following:
///     1. Initialize waffle with a platform and gl_api.
///     2. Create a context and window.
///     3. On the window call waffle_make_current, glClear,
///        and waffle_swap_buffers.
///     4. Verify the window contents with glReadPixels.
///     5. Tear down all waffle state.

#include <ctype.h>
#include <getopt.h>
#include <setjmp.h> // for cmocka.h
#include <stdarg.h> // for va_start, va_end
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <sys/types.h>
#if !defined(_WIN32)
#include <unistd.h>

#include <sys/wait.h>
#else
#include <windows.h>
#endif

#include "gl_basic_cocoa.h"
#include <cmocka.h>

#include "waffle.h"

enum {
    // Choosing a smaller window would shorten the execution time of pixel
    // validation, but Windows 7 enforces a minimum size.
    WINDOW_WIDTH = 320,
    WINDOW_HEIGHT = 240,
};

static const float RED_F = 1.00;
static const float GREEN_F = 0.00;
static const float BLUE_F = 1.00;
static const float ALPHA_F = 1.00;

static const uint8_t RED_UB = 0xff;
static const uint8_t GREEN_UB = 0x00;
static const uint8_t BLUE_UB = 0xff;
static const uint8_t ALPHA_UB = 0xff;

struct test_state_gl_basic {
    bool initialized;
    struct waffle_display *dpy;
    struct waffle_config *config;
    struct waffle_window *window;
    struct waffle_context *ctx;

    uint8_t actual_pixels[4 * WINDOW_WIDTH * WINDOW_HEIGHT];
    uint8_t expect_pixels[4 * WINDOW_WIDTH * WINDOW_HEIGHT];
};

#define ASSERT_GL(statement)                                                   \
    do {                                                                       \
        statement;                                                             \
        assert_false(glGetError());                                            \
    } while (0)

typedef unsigned int GLenum;
typedef unsigned char GLboolean;
typedef unsigned int GLbitfield;
typedef void GLvoid;
typedef signed char GLbyte;      /* 1-byte signed */
typedef short GLshort;           /* 2-byte signed */
typedef int GLint;               /* 4-byte signed */
typedef unsigned char GLubyte;   /* 1-byte unsigned */
typedef unsigned short GLushort; /* 2-byte unsigned */
typedef unsigned int GLuint;     /* 4-byte unsigned */
typedef int GLsizei;             /* 4-byte signed */
typedef float GLfloat;           /* single precision float */
typedef float GLclampf;          /* single precision float in [0,1] */
typedef double GLdouble;         /* double precision float */
typedef double GLclampd;         /* double precision float in [0,1] */

#define GL_NO_ERROR 0x0000
#define GL_VERSION 0x1F02
#define GL_UNSIGNED_BYTE 0x1401
#define GL_UNSIGNED_INT 0x1405
#define GL_FLOAT 0x1406
#define GL_RGB 0x1907
#define GL_RGBA 0x1908
#define GL_COLOR_BUFFER_BIT 0x00004000
#define GL_CONTEXT_FLAGS 0x821e
#define GL_CONTEXT_ROBUST_ACCESS 0x90F3

#define GL_CONTEXT_FLAG_FORWARD_COMPATIBLE_BIT 0x00000001
#define GL_CONTEXT_FLAG_DEBUG_BIT 0x00000002

#define GL_CONTEXT_PROFILE_MASK 0x9126
#define GL_CONTEXT_CORE_PROFILE_BIT 0x00000001
#define GL_CONTEXT_COMPATIBILITY_PROFILE_BIT 0x00000002

#ifndef _WIN32
#define APIENTRY
#else
#ifndef APIENTRY
#define APIENTRY __stdcall
#endif
#endif

static GLenum(APIENTRY *glGetError)(void);
static const GLubyte *(APIENTRY *glGetString)(GLenum name);
static void(APIENTRY *glGetIntegerv)(GLenum pname, GLint *params);
static void(APIENTRY *glClearColor)(GLclampf red,
                                    GLclampf green,
                                    GLclampf blue,
                                    GLclampf alpha);
static void(APIENTRY *glClear)(GLbitfield mask);
static void(APIENTRY *glReadPixels)(GLint x,
                                    GLint y,
                                    GLsizei width,
                                    GLsizei height,
                                    GLenum format,
                                    GLenum type,
                                    GLvoid *pixels);

static int
setup(void **state)
{
    struct test_state_gl_basic *ts;

    ts = calloc(1, sizeof(*ts));
    if (!ts)
        return -1;

    for (int y = 0; y < WINDOW_HEIGHT; ++y) {
        for (int x = 0; x < WINDOW_WIDTH; ++x) {
            uint8_t *p = &ts->expect_pixels[4 * (y * WINDOW_WIDTH + x)];
            p[0] = RED_UB;
            p[1] = GREEN_UB;
            p[2] = BLUE_UB;
            p[3] = ALPHA_UB;
        }
    }
    // Fill actual_pixels with canaries.
    memset(&ts->actual_pixels, 0x99, sizeof(ts->actual_pixels));

    *state = ts;
    return 0;
}

static int
teardown(void **state)
{
    free(*state);
    return 0;
}

// The rules that dictate how to properly query a GL symbol are complex. The
// rules depend on the OS, on the winsys API, and even on the particular driver
// being used. The rules differ between EGL 1.4 and EGL 1.5; differ between
// Linux, Windows, and Mac; and differ between Mesa and Mali.
//
// This function hides that complexity with a naive heuristic: try, then try
// again.
static void *
get_gl_symbol(enum waffle_enum context_api, const char *name)
{
    void *sym = NULL;
    enum waffle_enum dl = 0;

    switch (context_api) {
    case WAFFLE_CONTEXT_OPENGL:
        dl = WAFFLE_DL_OPENGL;
        break;
    case WAFFLE_CONTEXT_OPENGL_ES1:
        dl = WAFFLE_DL_OPENGL_ES1;
        break;
    case WAFFLE_CONTEXT_OPENGL_ES2:
        dl = WAFFLE_DL_OPENGL_ES2;
        break;
    case WAFFLE_CONTEXT_OPENGL_ES3:
        dl = WAFFLE_DL_OPENGL_ES3;
        break;
    default:
        assert_true(0);
        break;
    }

    if (waffle_dl_can_open(dl)) {
        sym = waffle_dl_sym(dl, name);
    }

    if (!sym) {
        sym = waffle_get_proc_address(name);
    }

    return sym;
}

static int32_t waffle_platform;
static const char *platform;

static void
error_waffle(void);

static int
gl_basic_init(void **state)
{
    struct test_state_gl_basic *ts;
    int ret;

    ret = setup((void **)&ts);
    if (ret)
        return -1;

    const int32_t init_attrib_list[] = {
        WAFFLE_PLATFORM,
        waffle_platform,
        0,
    };

    ts->initialized = waffle_init(init_attrib_list);
    if (!ts->initialized) {
        error_waffle();
        teardown(state);
        return -1;
    }

    *state = ts;
    return 0;
}

static int
gl_basic_fini(void **state)
{
    struct test_state_gl_basic *ts = *state;
    bool ret = false;

    // XXX: return immediately on error or attempt to finish the teardown ?
    if (ts->dpy) // XXX: keep track if we've had current ctx ?
        ret = waffle_make_current(ts->dpy, NULL, NULL);
    if (ts->window)
        ret = waffle_window_destroy(ts->window);
    if (ts->ctx)
        ret = waffle_context_destroy(ts->ctx);
    if (ts->config)
        ret = waffle_config_destroy(ts->config);
    if (ts->dpy)
        ret = waffle_display_disconnect(ts->dpy);

    if (ts->initialized)
        ret = waffle_teardown();

    teardown(state);
    return ret ? 0 : -1;
}

enum {
    TEST_CTX_FWDCOMPAT = 1 << 0,
    TEST_CTX_DEBUG = 1 << 1,
    TEST_CTX_ROBUST = 1 << 2,
    TEST_CTX_LOSE_CONTEXT = 1 << 3,
};

#define gl_basic_draw(state, ...)                                              \
                                                                               \
    gl_basic_draw__(                                                           \
        state, (struct gl_basic_draw_args__){ .api = 0,                        \
                                              .version = WAFFLE_DONT_CARE,     \
                                              .profile = WAFFLE_DONT_CARE,     \
                                              .expect_error = WAFFLE_NO_ERROR, \
                                              .context_flags = 0,              \
                                              .alpha = false,                  \
                                              __VA_ARGS__ })

struct gl_basic_draw_args__ {
    int32_t api;
    int32_t version;
    int32_t profile;
    int32_t expect_error;
    uint32_t context_flags;
    bool alpha;
};

#define assert_int_equal_print(_a, _b)                                         \
    if (_a != _b) {                                                            \
        fprintf(stderr, #_a " (%d) != " #_b "(%d)\n", _a, _b);                 \
        assert_true(0 && "Integer comparison failed");                         \
    }

#define assert_int_ge(_a, _b)                                                  \
    if (_a < _b) {                                                             \
        fprintf(stderr, #_a " (%d) < " #_b "(%d)\n", _a, _b);                  \
        assert_true(0 && "Integer comparison failed");                         \
    }

#define assert_in_range_print(_a, _min, _max)                                  \
    if (_a < _min || _a > _max) {                                              \
        fprintf(stderr,                                                        \
                #_a " (%d) outside range " #_min "(%d) - " #_max "(%d)\n", _a, \
                _min, _max);                                                   \
        assert_true(0 && "Integer comparison failed");                         \
    }

#define assert_string_len_equal(_actual, _expected, _len)                      \
    if (strncmp(_actual, _expected, _len) != 0) {                              \
        fprintf(stderr, "Expected (" #_expected "): \"%.*s\"\n", (int)_len,    \
                _expected);                                                    \
        fprintf(stderr, "Received (" #_actual "): \"%.*s\"\n", (int)_len,      \
                _actual);                                                      \
        assert_true(0 && "String comparison failed");                          \
    }

static void
error_waffle(void)
{
    const struct waffle_error_info *info = waffle_error_get_info();
    const char *code = waffle_error_to_string(info->code);

    if (info->message_length > 0)
        fprintf(stderr, "Waffle error: 0x%x %s: %s\n", info->code, code,
                info->message);
    else
        fprintf(stderr, "Waffle error: 0x%x %s\n", info->code, code);
}

#define assert_true_with_wfl_error(_arg)                                       \
    if (!(_arg)) {                                                             \
        error_waffle();                                                        \
        assert_true(0);                                                        \
    }

static void
gl_basic_draw__(void **state, struct gl_basic_draw_args__ args)
{
    struct test_state_gl_basic *ts = *state;
    int32_t waffle_context_api = args.api;
    int32_t context_version = args.version;
    int32_t context_profile = args.profile;
    int32_t expect_error = args.expect_error;
    bool context_forward_compatible = args.context_flags & TEST_CTX_FWDCOMPAT;
    bool context_debug = args.context_flags & TEST_CTX_DEBUG;
    bool context_robust = args.context_flags & TEST_CTX_ROBUST;
    bool lose_context_on_reset = args.context_flags & TEST_CTX_LOSE_CONTEXT;
    bool alpha = args.alpha;
    bool ret;

    int32_t config_attrib_list[64];
    int i;

    const intptr_t window_attrib_list[] = {
        WAFFLE_WINDOW_WIDTH,
        WINDOW_WIDTH,
        WAFFLE_WINDOW_HEIGHT,
        WINDOW_HEIGHT,
        0,
    };

    i = 0;
    config_attrib_list[i++] = WAFFLE_CONTEXT_API;
    config_attrib_list[i++] = waffle_context_api;
    if (context_version != WAFFLE_DONT_CARE) {
        config_attrib_list[i++] = WAFFLE_CONTEXT_MAJOR_VERSION;
        config_attrib_list[i++] = context_version / 10;
        config_attrib_list[i++] = WAFFLE_CONTEXT_MINOR_VERSION;
        config_attrib_list[i++] = context_version % 10;
    }
    if (context_profile != WAFFLE_DONT_CARE) {
        config_attrib_list[i++] = WAFFLE_CONTEXT_PROFILE;
        config_attrib_list[i++] = context_profile;
    }
    if (context_forward_compatible) {
        config_attrib_list[i++] = WAFFLE_CONTEXT_FORWARD_COMPATIBLE;
        config_attrib_list[i++] = true;
    }
    if (context_debug) {
        config_attrib_list[i++] = WAFFLE_CONTEXT_DEBUG;
        config_attrib_list[i++] = true;
    }
    if (context_robust) {
        config_attrib_list[i++] = WAFFLE_CONTEXT_ROBUST_ACCESS;
        config_attrib_list[i++] = true;
    }
    if (lose_context_on_reset) {
        config_attrib_list[i++] = WAFFLE_CONTEXT_LOSE_CONTEXT_ON_RESET;
        config_attrib_list[i++] = true;
    }
    config_attrib_list[i++] = WAFFLE_RED_SIZE;
    config_attrib_list[i++] = 8;
    config_attrib_list[i++] = WAFFLE_GREEN_SIZE;
    config_attrib_list[i++] = 8;
    config_attrib_list[i++] = WAFFLE_BLUE_SIZE;
    config_attrib_list[i++] = 8;
    config_attrib_list[i++] = WAFFLE_ALPHA_SIZE;
    config_attrib_list[i++] = alpha;
    config_attrib_list[i++] = 0;

    // Create objects.
    ts->dpy = waffle_display_connect(NULL);
    assert_true_with_wfl_error(ts->dpy);

    ts->config = waffle_config_choose(ts->dpy, config_attrib_list);
    if (expect_error) {
        assert_true(ts->config == NULL);
        assert_true((int32_t)waffle_error_get_code() == expect_error);
        return;
    } else if (ts->config == NULL) {
        switch (waffle_error_get_code()) {
        case WAFFLE_ERROR_UNSUPPORTED_ON_PLATFORM:
            // fall-through
        case WAFFLE_ERROR_UNKNOWN:
            // Assume that the native platform rejected the requested
            // config flavor.
            skip();
            // XXX: skip() is not annotated as noreturn, leading to compiler
            // warning about implicit fallthrough
            break;
        default:
            assert_true_with_wfl_error(ts->config);
        }
    }

    ts->window = waffle_window_create2(ts->config, window_attrib_list);
    assert_true_with_wfl_error(ts->window);

    ret = waffle_window_show(ts->window);
    assert_true_with_wfl_error(ret);

    ts->ctx = waffle_context_create(ts->config, NULL);
    if (ts->ctx == NULL) {
        switch (waffle_error_get_code()) {
        case WAFFLE_ERROR_UNSUPPORTED_ON_PLATFORM:
            // fall-through
        case WAFFLE_ERROR_UNKNOWN:
            // Assume that the native platform rejected the requested
            // context flavor.
            skip();
            // XXX: skip() is not annotated as noreturn, leading to compiler
            // warning about implicit fallthrough
            break;
        default:
            assert_true_with_wfl_error(ts->ctx);
        }
    }

    // Get OpenGL functions.
    // clang-format off
    assert_true(glClear         = get_gl_symbol(waffle_context_api, "glClear"));
    assert_true(glClearColor    = get_gl_symbol(waffle_context_api, "glClearColor"));
    assert_true(glGetError      = get_gl_symbol(waffle_context_api, "glGetError"));
    assert_true(glGetIntegerv   = get_gl_symbol(waffle_context_api, "glGetIntegerv"));
    assert_true(glReadPixels    = get_gl_symbol(waffle_context_api, "glReadPixels"));
    assert_true(glGetString     = get_gl_symbol(waffle_context_api, "glGetString"));
    // clang-format on

    ret = waffle_make_current(ts->dpy, ts->window, ts->ctx);
    assert_true_with_wfl_error(ret);

    assert_true(waffle_get_current_display() == ts->dpy);
    assert_true(waffle_get_current_window() == ts->window);
    assert_true(waffle_get_current_context() == ts->ctx);

    const char *version_str, *expected_version_str;
    int major, minor, count;

    ASSERT_GL(version_str = (const char *)glGetString(GL_VERSION));
    assert_true(version_str != NULL);

    switch (waffle_context_api) {
    case WAFFLE_CONTEXT_OPENGL:
        expected_version_str = "";
        break;
    case WAFFLE_CONTEXT_OPENGL_ES1:
        expected_version_str = "OpenGL ES-CM ";
        break;
    case WAFFLE_CONTEXT_OPENGL_ES2:
    case WAFFLE_CONTEXT_OPENGL_ES3:
        expected_version_str = "OpenGL ES ";
        break;
    default:
        // Explicitly initialize, to appease GCC
        expected_version_str = "";
        assert_true(0);
        break;
    }

    const size_t version_str_len = strlen(expected_version_str);
    assert_string_len_equal(version_str, expected_version_str, version_str_len);
    version_str += version_str_len;

    count = sscanf(version_str, "%d.%d", &major, &minor);
    assert_int_equal_print(count, 2);
    assert_int_ge(major, 0);
    assert_in_range_print(minor, 0, 10);

    if (context_version != WAFFLE_DONT_CARE) {
        int expected_major = context_version / 10;
        int expected_minor = context_version % 10;

        assert_int_ge(major, expected_major);
        if (major == expected_major)
            assert_int_ge(minor, expected_minor);
    }

    const char *profile_suffix = "";

    if (waffle_context_api == WAFFLE_CONTEXT_OPENGL) {
        switch (context_profile) {
        case WAFFLE_CONTEXT_CORE_PROFILE:
            profile_suffix = " (Core Profile)";
#ifdef __APPLE__
            fprintf(stderr,
                    "MacOS Core contexts, omit the \"%s\" suffix in "
                    "glGetString(GL_VERSION)."
                    "Applying workaround.\n",
                    profile_suffix);
            profile_suffix = "";
#endif
            break;
        case WAFFLE_CONTEXT_COMPATIBILITY_PROFILE:
            // HACK: seems like Mesa 19.3.3 at least will report
            if (context_forward_compatible)
                profile_suffix = " (Core Profile)";
            else
                profile_suffix = " (Compatibility Profile)";
            break;
        case WAFFLE_DONT_CARE:
            break;
        default:
            assert_true(0);
            break;
        }
    }

    char profile_str[30]; // 30 should be enough ;-)
    sprintf(profile_str, "%d.%d%s", major, minor, profile_suffix);

    const size_t profile_str_len = strlen(profile_str);
    assert_string_len_equal(version_str, profile_str, profile_str_len);

    int version_10x = 10 * major + minor;

    // Another profile check
    if (waffle_context_api == WAFFLE_CONTEXT_OPENGL && version_10x >= 32) {
        GLint profile_mask = 0;
        glGetIntegerv(GL_CONTEXT_PROFILE_MASK, &profile_mask);

        switch (context_profile) {
        case WAFFLE_CONTEXT_CORE_PROFILE:
            assert_true(profile_mask & GL_CONTEXT_CORE_PROFILE_BIT);
            break;
        case WAFFLE_CONTEXT_COMPATIBILITY_PROFILE:
            // HACK: seems like Mesa 19.3.3 at least will report
            if (context_forward_compatible)
                assert_true(profile_mask & GL_CONTEXT_CORE_PROFILE_BIT);
            else
                assert_true(profile_mask &
                            GL_CONTEXT_COMPATIBILITY_PROFILE_BIT);
            break;
        case WAFFLE_DONT_CARE:
            break;
        default:
            assert_true(0);
            break;
        }
    }

    if ((waffle_context_api == WAFFLE_CONTEXT_OPENGL && version_10x >= 30) ||
        (waffle_context_api != WAFFLE_CONTEXT_OPENGL && version_10x >= 32)) {
        GLint context_flags = 0;
        if (context_forward_compatible || context_debug) {
            glGetIntegerv(GL_CONTEXT_FLAGS, &context_flags);
        }

        if (context_forward_compatible) {
            assert_true(context_flags & GL_CONTEXT_FLAG_FORWARD_COMPATIBLE_BIT);
        }

        if (context_debug) {
            assert_true(context_flags & GL_CONTEXT_FLAG_DEBUG_BIT);
        }
    }

    // GL_ROBUST_ACCESS comes with the following extensions
    // GL_ARB_robustness
    // GL_EXT_robustness
    // GL_KHR_robustness
    //
    // To keep it simple, assume the correct extension is there if
    // glGetError is happy ;-)
    if (context_robust) {
        GLint robust_flag = 0;
        glGetIntegerv(GL_CONTEXT_ROBUST_ACCESS, &robust_flag);

        if (glGetError() == GL_NO_ERROR)
            assert_true(robust_flag);
    }

    // Draw.
    ASSERT_GL(glClearColor(RED_F, GREEN_F, BLUE_F, ALPHA_F));
    ASSERT_GL(glClear(GL_COLOR_BUFFER_BIT));
    ASSERT_GL(glReadPixels(0, 0, WINDOW_WIDTH, WINDOW_HEIGHT, GL_RGBA,
                           GL_UNSIGNED_BYTE, ts->actual_pixels));
    ret = waffle_window_swap_buffers(ts->window);
    assert_true_with_wfl_error(ret);

    assert_memory_equal(&ts->actual_pixels, &ts->expect_pixels,
                        sizeof(ts->expect_pixels));
}

//
// List of tests common to all platforms.
//

// clang-format off

#define test_rgb(waffle_api)                                            \
static void waffle_api##_rgb(void **state)                              \
{                                                                       \
    gl_basic_draw(state,                                                \
                  .api=WAFFLE_CONTEXT_##waffle_api);                    \
}

#define test_rgba(waffle_api)                                           \
static void waffle_api##_rgba(void **state)                             \
{                                                                       \
    gl_basic_draw(state,                                                \
                  .api=WAFFLE_CONTEXT_##waffle_api,                     \
                  .alpha=true);                                         \
}

#define test_context_flags(waffle_api, waffle_flags)                    \
static void waffle_api##_##waffle_flags(void **state)                   \
{                                                                       \
    int32_t expect_error = TEST_##waffle_flags & TEST_CTX_FWDCOMPAT ?   \
                           WAFFLE_ERROR_BAD_ATTRIBUTE : WAFFLE_NO_ERROR; \
    gl_basic_draw(state,                                                \
                  .api=WAFFLE_CONTEXT_##waffle_api,                     \
                  .context_flags=TEST_##waffle_flags,                   \
                  .expect_error=expect_error);                          \
}

#define test_version(waffle_api, waffle_version)                        \
static void waffle_api##_##waffle_version(void **state)                 \
{                                                                       \
    gl_basic_draw(state,                                                \
                  .api=WAFFLE_CONTEXT_##waffle_api,                     \
                  .version=waffle_version);                             \
}

#define test_version_fwdcompat(waffle_api, waffle_version)              \
static void waffle_api##_fwdcompat_##waffle_version(void **state)       \
{                                                                       \
    gl_basic_draw(state,                                                \
                  .api=WAFFLE_CONTEXT_##waffle_api,                     \
                  .version=waffle_version,                              \
                  .context_flags=TEST_CTX_FWDCOMPAT);                   \
}

#define test_profile_version(waffle_profile, waffle_version)            \
static void waffle_profile##_##waffle_version(void **state)             \
{                                                                       \
    gl_basic_draw(state,                                                \
                  .api=WAFFLE_CONTEXT_OPENGL,                           \
                  .version=waffle_version,                              \
                  .profile=WAFFLE_CONTEXT_##waffle_profile##_PROFILE);  \
}

#define test_profile_context_flags(waffle_profile, waffle_flags)        \
static void waffle_profile##_##waffle_flags(void **state)               \
{                                                                       \
    gl_basic_draw(state,                                                \
                  .api=WAFFLE_CONTEXT_OPENGL,                           \
                  .version=32,                                          \
                  .profile=WAFFLE_CONTEXT_##waffle_profile##_PROFILE,   \
                  .context_flags=TEST_##waffle_flags);                  \
}


//
// Most of the following tests will return ERROR_UNSUPPORTED_ON_PLATFORM
// on Apple/CGL, where NO_ERROR is expected.
// This is safe, as the test is skipped when the said error occurs.
//
// As BAD_ATTRIBUTE (core validation) takes greater precedence over
// UNSUPPORTED_ON_PLATFORM (platform specific one).
// Thus we're safe to use the former here, eventhough CGL has support
// for neither GLES* nor fwdcompa "CGL and everyone else".
//

//
// Quick notes which combinations we test and why
// - w/o version, for each API/profile combination check variations of
//   - visual - make sure we can draw fine, duh
//   - flags - flags selection machinery is funky
// - fwdcompat was introduced with OpenGL 3.0, check it and 3.1
// - specific version
//   - all known versions
//   - no permutation
//


// OPENGL, no profile

test_rgb(OPENGL)
test_rgba(OPENGL)

test_context_flags(OPENGL, CTX_FWDCOMPAT)
test_context_flags(OPENGL, CTX_DEBUG)
test_context_flags(OPENGL, CTX_ROBUST)
test_context_flags(OPENGL, CTX_LOSE_CONTEXT)

test_version_fwdcompat(OPENGL, 30)
test_version_fwdcompat(OPENGL, 31)

test_version(OPENGL, 10)
test_version(OPENGL, 11)
test_version(OPENGL, 12)
test_version(OPENGL, 13)
test_version(OPENGL, 14)
test_version(OPENGL, 15)
test_version(OPENGL, 20)
test_version(OPENGL, 21)
test_version(OPENGL, 30)
test_version(OPENGL, 31)

// OPENGL, CORE profile

test_profile_context_flags(CORE, CTX_FWDCOMPAT)
test_profile_context_flags(CORE, CTX_DEBUG)
test_profile_context_flags(CORE, CTX_ROBUST)
test_profile_context_flags(CORE, CTX_LOSE_CONTEXT)

test_profile_version(CORE, 32)
test_profile_version(CORE, 33)
test_profile_version(CORE, 40)
test_profile_version(CORE, 41)
test_profile_version(CORE, 42)
test_profile_version(CORE, 43)
test_profile_version(CORE, 44)
test_profile_version(CORE, 45)
test_profile_version(CORE, 46)

// OPENGL, COMPATIBILITY profile

test_profile_context_flags(COMPATIBILITY, CTX_FWDCOMPAT)
test_profile_context_flags(COMPATIBILITY, CTX_DEBUG)
test_profile_context_flags(COMPATIBILITY, CTX_ROBUST)
test_profile_context_flags(COMPATIBILITY, CTX_LOSE_CONTEXT)

test_profile_version(COMPATIBILITY, 32)
test_profile_version(COMPATIBILITY, 33)
test_profile_version(COMPATIBILITY, 40)
test_profile_version(COMPATIBILITY, 41)
test_profile_version(COMPATIBILITY, 42)
test_profile_version(COMPATIBILITY, 43)
test_profile_version(COMPATIBILITY, 44)
test_profile_version(COMPATIBILITY, 45)
test_profile_version(COMPATIBILITY, 46)

// OPENGL_ES1

test_rgb(OPENGL_ES1)
test_rgba(OPENGL_ES1)

test_context_flags(OPENGL_ES1, CTX_FWDCOMPAT)
test_context_flags(OPENGL_ES1, CTX_DEBUG)
test_context_flags(OPENGL_ES1, CTX_ROBUST)
test_context_flags(OPENGL_ES1, CTX_LOSE_CONTEXT)

test_version(OPENGL_ES1, 10)
test_version(OPENGL_ES1, 11)

// OPENGL_ES2

test_rgb(OPENGL_ES2)
test_rgba(OPENGL_ES2)

test_context_flags(OPENGL_ES2, CTX_FWDCOMPAT)
test_context_flags(OPENGL_ES2, CTX_DEBUG)
test_context_flags(OPENGL_ES2, CTX_ROBUST)
test_context_flags(OPENGL_ES2, CTX_LOSE_CONTEXT)

test_version(OPENGL_ES2, 20)

// OPENGL_ES3

test_rgb(OPENGL_ES3)
test_rgba(OPENGL_ES3)

test_context_flags(OPENGL_ES3, CTX_FWDCOMPAT)
test_context_flags(OPENGL_ES3, CTX_DEBUG)
test_context_flags(OPENGL_ES3, CTX_ROBUST)
test_context_flags(OPENGL_ES3, CTX_LOSE_CONTEXT)

test_version(OPENGL_ES3, 30)
test_version(OPENGL_ES3, 31)
test_version(OPENGL_ES3, 32)


#ifdef __APPLE__

static void
removeArg(int index, int *argc, char **argv)
// clang-format on
{
    --*argc;
    for (; index < *argc; ++index)
        argv[index] = argv[index + 1];
}

static void
removeXcodeArgs(int *argc, char **argv)
{
    // Xcode sometimes adds additional arguments.
    for (int i = 1; i < *argc;) {
        if (strcmp(argv[i], "-NSDocumentRevisionsDebugMode") == 0 ||
            strcmp(argv[i], "-ApplePersistenceIgnoreState") == 0) {
            removeArg(i, argc, argv);
            removeArg(i, argc, argv);
        } else
            ++i;
    }
}

#endif // __APPLE__

static const char *usage_message =
    "Usage:\n"
    "    gl_basic_test <Required Parameter> [Options]\n"
    "\n"
    "Description:\n"
    "    Run the basic functionality tests.\n"
    "\n"
    "Required Parameter:\n"
    "    -p, --platform\n"
    "        One of: cgl, gbm, glx, wayland, wgl or x11_egl\n"
    "\n"
    "Options:\n"
    "    -h, --help\n"
    "        Print gl_basic_test usage information.\n";

#if defined(__GNUC__)
#define NORETURN __attribute__((noreturn))
#elif defined(_MSC_VER)
#define NORETURN __declspec(noreturn)
#else
#define NORETURN
#endif

#if defined(__clang__)
#define PRINTFLIKE(f, a) __attribute__((format(printf, f, a)))
#elif defined(__GNUC__)
#define PRINTFLIKE(f, a) __attribute__((format(gnu_printf, f, a)))
#else
#define PRINTFLIKE(f, a)
#endif

static void NORETURN
write_usage_and_exit(FILE *f, int exit_code)
{
    fprintf(f, "%s", usage_message);
    exit(exit_code);
}

enum {
    OPT_PLATFORM = 'p',
    OPT_HELP = 'h',
};

static const struct option get_opts[] = {
    { .name = "platform", .has_arg = required_argument, .val = OPT_PLATFORM },
    { .name = "help", .has_arg = no_argument, .val = OPT_HELP },
};

// clang-format off
static void NORETURN PRINTFLIKE(1, 2)
usage_error_printf(const char *fmt, ...)
// clang-format on
{
    fprintf(stderr, "gl_basic_test usage error: ");

    if (fmt) {
        va_list ap;
        va_start(ap, fmt);
        vfprintf(stderr, fmt, ap);
        va_end(ap);
        fprintf(stderr, " ");
    }

    fprintf(stderr, "(see gl_basic_test --help)\n");
    exit(EXIT_FAILURE);
}

struct enum_map {
    int i;
    const char *s;
};

static const struct enum_map platform_map[] = {
    { WAFFLE_PLATFORM_CGL, "cgl" },
    { WAFFLE_PLATFORM_GBM, "gbm" },
    { WAFFLE_PLATFORM_GLX, "glx" },
    { WAFFLE_PLATFORM_WAYLAND, "wayland" },
    { WAFFLE_PLATFORM_WGL, "wgl" },
    { WAFFLE_PLATFORM_X11_EGL, "x11_egl" },
    { WAFFLE_PLATFORM_SURFACELESS_EGL, "surfaceless_egl" },
    { WAFFLE_PLATFORM_SURFACELESS_EGL, "sl" },
    { 0, 0 },
};

/// @brief Translate string to `enum waffle_enum`.
///
/// @param self is a list of map items. The last item must be zero-filled.
/// @param result is altered only if @a s if found.
/// @return true if @a s was found in @a map.
static bool
enum_map_translate_str(const struct enum_map *self, const char *s, int *result)
{
    for (const struct enum_map *i = self; i->i != 0; ++i) {
        if (!strncmp(s, i->s, strlen(i->s) + 1)) {
            *result = i->i;
            return true;
        }
    }

    return false;
}

/// @return true on success.
static void
parse_args(int argc, char *argv[])
{
    bool loop_get_opt = true;

#ifdef __APPLE__
    removeXcodeArgs(&argc, argv);
#endif

    // prevent getopt_long from printing an error message
    opterr = 0;

    while (loop_get_opt) {
        int opt = getopt_long(argc, argv, "hp:", get_opts, NULL);
        switch (opt) {
        case -1:
            loop_get_opt = false;
            break;
        case '?':
            goto error_unrecognized_arg;
        case OPT_PLATFORM:
            if (!enum_map_translate_str(platform_map, optarg,
                                        &waffle_platform)) {
                usage_error_printf("'%s' is not a valid platform", optarg);
            }
            platform = optarg;
            break;
        case OPT_HELP:
            write_usage_and_exit(stdout, EXIT_SUCCESS);
            break;
        default:
            loop_get_opt = false;
            break;
        }
    }

    if (optind < argc) {
        goto error_unrecognized_arg;
    }

    if (!waffle_platform) {
        usage_error_printf("--platform is required");
    }

    return;

error_unrecognized_arg:
    if (optarg)
        usage_error_printf("unrecognized option '%s'", optarg);
    else if (optopt)
        usage_error_printf("unrecognized option '-%c'", optopt);
    else
        usage_error_printf("unrecognized option");
}

int
main(int argc, char *argv[])
{
    parse_args(argc, argv);

#define unit_test_make(name)                                                   \
    cmocka_unit_test_setup_teardown(name, gl_basic_init, gl_basic_fini)

    const struct CMUnitTest tests[] = {

        // OPENGL, no profile

        unit_test_make(OPENGL_rgb),
        unit_test_make(OPENGL_rgba),

        unit_test_make(OPENGL_CTX_FWDCOMPAT),
        unit_test_make(OPENGL_CTX_DEBUG),
        unit_test_make(OPENGL_CTX_ROBUST),
        unit_test_make(OPENGL_CTX_LOSE_CONTEXT),

        unit_test_make(OPENGL_fwdcompat_30),
        unit_test_make(OPENGL_fwdcompat_31),

        unit_test_make(OPENGL_10),
        unit_test_make(OPENGL_11),
        unit_test_make(OPENGL_12),
        unit_test_make(OPENGL_13),
        unit_test_make(OPENGL_14),
        unit_test_make(OPENGL_15),
        unit_test_make(OPENGL_20),
        unit_test_make(OPENGL_21),
        unit_test_make(OPENGL_30),
        unit_test_make(OPENGL_31),

        // OPENGL, CORE profile

        unit_test_make(CORE_CTX_FWDCOMPAT),
        unit_test_make(CORE_CTX_DEBUG),
        unit_test_make(CORE_CTX_ROBUST),
        unit_test_make(CORE_CTX_LOSE_CONTEXT),

        unit_test_make(CORE_32),
        unit_test_make(CORE_33),
        unit_test_make(CORE_40),
        unit_test_make(CORE_41),
        unit_test_make(CORE_42),
        unit_test_make(CORE_43),
        unit_test_make(CORE_44),
        unit_test_make(CORE_45),
        unit_test_make(CORE_46),

        // OPENGL, COMPATIBILITY profile

        unit_test_make(COMPATIBILITY_CTX_FWDCOMPAT),
        unit_test_make(COMPATIBILITY_CTX_DEBUG),
        unit_test_make(COMPATIBILITY_CTX_ROBUST),
        unit_test_make(COMPATIBILITY_CTX_LOSE_CONTEXT),

        unit_test_make(COMPATIBILITY_32),
        unit_test_make(COMPATIBILITY_33),
        unit_test_make(COMPATIBILITY_40),
        unit_test_make(COMPATIBILITY_41),
        unit_test_make(COMPATIBILITY_42),
        unit_test_make(COMPATIBILITY_43),
        unit_test_make(COMPATIBILITY_44),
        unit_test_make(COMPATIBILITY_45),
        unit_test_make(COMPATIBILITY_46),

        // OPENGL_ES1

        unit_test_make(OPENGL_ES1_rgb),
        unit_test_make(OPENGL_ES1_rgba),

        unit_test_make(OPENGL_ES1_CTX_FWDCOMPAT),
        unit_test_make(OPENGL_ES1_CTX_DEBUG),
        unit_test_make(OPENGL_ES1_CTX_ROBUST),
        unit_test_make(OPENGL_ES1_CTX_LOSE_CONTEXT),

        unit_test_make(OPENGL_ES1_10),
        unit_test_make(OPENGL_ES1_11),

        // OPENGL_ES2

        unit_test_make(OPENGL_ES2_rgb),
        unit_test_make(OPENGL_ES2_rgba),

        unit_test_make(OPENGL_ES2_CTX_FWDCOMPAT),
        unit_test_make(OPENGL_ES2_CTX_DEBUG),
        unit_test_make(OPENGL_ES2_CTX_ROBUST),
        unit_test_make(OPENGL_ES2_CTX_LOSE_CONTEXT),

        unit_test_make(OPENGL_ES2_20),

        // OPENGL_ES3

        unit_test_make(OPENGL_ES3_rgb),
        unit_test_make(OPENGL_ES3_rgba),

        unit_test_make(OPENGL_ES3_CTX_FWDCOMPAT),
        unit_test_make(OPENGL_ES3_CTX_DEBUG),
        unit_test_make(OPENGL_ES3_CTX_ROBUST),
        unit_test_make(OPENGL_ES3_CTX_LOSE_CONTEXT),

        unit_test_make(OPENGL_ES3_30),
        unit_test_make(OPENGL_ES3_31),
        unit_test_make(OPENGL_ES3_32),
    };

    return cmocka_run_group_tests_name(platform, tests, NULL, NULL);
}
